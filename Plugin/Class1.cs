﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GamePlay;
using System.Drawing;

namespace Plugin
{
    public class Class1 : AIPlugin
    {
        public override string Name {
            get {
                return "Easy";
            }
        }

        public override Point ptNextMove(CellContent[,] ccCurrentBoard, CellContent ccCurrentPlayer) {

            int iRow = 0;
            int iCol = 0;
            for(iRow = 0; iRow < 3; iRow++) {
                int iCount = 0;
                int iEmptyCol = -1;
                for(iCol = 0; iCol < 3; iCol++) {
                    if(ccCurrentBoard[iRow, iCol] == CellContent.Empty) {
                        iEmptyCol = iCol;
                    } else if(ccCurrentBoard[iRow, iCol] == ccCurrentPlayer) {
                        iCount++;
                    }
                }
                if(iCount == 2 && iEmptyCol != -1) {
                    return new Point(iRow, iEmptyCol);
                }
            }

            iRow = 0;
            iCol = 0;
            for (iCol = 0; iCol < 3; iCol++) {
                int iCount = 0;
                int iEmptyRow = -1;
                for (iRow = 0; iRow < 3; iRow++) {
                    if (ccCurrentBoard[iRow, iCol] == CellContent.Empty) {
                        iEmptyRow = iRow;
                    } else if (ccCurrentBoard[iRow, iCol] == ccCurrentPlayer) {
                        iCount++;
                    }
                }
                if (iCount == 2 && iEmptyRow != -1) {
                    return new Point(iEmptyRow, iCol);
                }
            }

            iRow = 0;
            iCol = 0;
            for (iRow = 0; iRow < 3; iRow++) {
                int iEmpty = -1;
                int iCount = 0;
                for (iCol = 0; iCol < 3; iCol++) {
                    if (ccCurrentBoard[iRow, iCol] == CellContent.Empty) {
                        iEmpty = iRow;
                    } else if (ccCurrentBoard[iRow, iCol] == ccCurrentPlayer) {
                        iCount++;
                    }
                }
                if (iCount == 2 && iEmpty != -1) {
                    return new Point(iEmpty, iCol);
                }
            
            }


            iRow = 0;
            iCol = 3;
            for (iRow = 0; iRow < 3; iRow++) {
                int iEmpty = -1;
                int iCount = 0;
                for (iCol = 3; iCol > 0; iCol--) {
                    if (ccCurrentBoard[iRow, iCol] == CellContent.Empty) {
                        iEmpty = iRow;
                    } else if (ccCurrentBoard[iRow, iCol] == ccCurrentPlayer) {
                        iCount++;
                    }
                }
                if (iCount == 2 && iEmpty != -1) {
                    return new Point(iEmpty, iCol);
                }
            
            }


                Random rng = new Random();
            iRow = rng.Next(0, 3);
            iCol = rng.Next(0, 3);
            while (ccCurrentBoard[iRow, iCol] != CellContent.Empty) {
                iRow = rng.Next(0, 3);
                iCol = rng.Next(0, 3);
            }
            return new Point(iRow, iCol);
        }
    }
}
