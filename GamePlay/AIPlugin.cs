﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GamePlay {
    public abstract class AIPlugin {
        public abstract string Name {
            get;
        }

        public abstract Point ptNextMove(CellContent[,] ccCurrentBoard, CellContent ccCurrentPlayer);
    }
}
